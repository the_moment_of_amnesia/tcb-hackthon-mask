// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
exports.main = async (event, context) => {
	const wxContext = cloud.getWXContext()
	var $ = db.command.aggregate
	var list = await db.collection('appoint').aggregate().lookup({
		from: 'shop',
		localField: 'shop_id',
		foreignField: '_id',
		as: 'shop',
	}).lookup({
		from: 'timeline',
		localField: 'timeline_id',
		foreignField: '_id',
		as: 'timeline',
	}).lookup({
		from: 'daily',
		localField: 'daily_id',
		foreignField: '_id',
		as: 'daily',
	}).lookup({
		from: 'product',
		localField: 'product_id',
		foreignField: '_id',
		as: 'product',
	}).match({
		openid: wxContext.OPENID
	}).skip(event.skip).limit(15).sort({
		'create_time': -1
	}).project({
		_id: 1,
		group_id: 1,
		name: 1,
		avatarUrl: 1,
		'shop.name':1,
		'daily.date':1,
		'product.name':1,
		'daily.date':1,
		'timeline.time_begin':1,
		'timeline.time_end':1,
		status:1
	}).end().catch(err => console.error(err))
	return {
		code:200,
		data:list.list
	}

}