const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database()
exports.main = async (event, context) => {
	const wxContext = cloud.getWXContext()
	let staff = await db.collection('staff').where({
		_openid:wxContext.OPENID,
		shop_id:event._id
	}).get()
	if(staff.data.length==0){
		return {
			code: 20003,
			msg: '您无权进行此操作'
		}
	}
	staff = staff.data[0]
	console.log(staff)
	if (staff.group_id == 3) {
		return {
			code: 20003,
			msg: '您无权进行此操作'
		}
	}
	let shop = await db.collection('shop').doc(event._id).field({
		_id:true,
		status:true,
		name:true
	}).get()
	shop = shop.data
	console.log(shop)
	if(!shop){
		return {
			code:20002,
			msg:'门店不存在'
		}
	}
	if(shop.status!=2){
		return {
			code:20004,
			msg:'门店尚未审核通过'
		}
	}
	return {
		code:200,
		data:{
			shop,
			staff
		}
	}
}