const app = getApp()
Page({

	data: {
		result: '加载中......'
	},

	onLoad: function (options) {
		if(!options.scene){
			return this.setData({
				result: '缺少参数'
			})
		}
		app.db.collection('shop').where({
				_id: options.scene,
				status: 2,
				is_visit: true
			})
			.field({
				_id: true,
				address: true,
				content: true,
				cover_image: true,
				location: true,
				name: true,
				phone: true
			})
			.get()
			.then(res => {
				if (res.data.length == 0) {
					return this.setData({
						result: '门店不存在或未上线'
					})
				}
				this.setData({
					result: ''
				})
				wx.setNavigationBarTitle({
					title: res.data[0].name,
				})
				this.setData(res.data[0])
				if (this.data.cover_image) {
					wx.cloud.getTempFileURL({
						fileList: [this.data.cover_image]
					}).then(res => {
						this.setData({
							cover_image: res.fileList[0]
						})
					})
				}
				//开始获取项目
				app.db.collection('product').where({
					shop_id:this.data._id,
					is_show:true
				}).field({
					_id:true,
					name:true,
					price:true,
					to_home:true,
					to_shop:true,
				}).orderBy('create_time','asc').get().then(res=>{
					this.setData({products:res.data})
				})
			})
	},

	onShareAppMessage: function () {
		return {
			title: this.data.name,
			path: '/pages/shop/index?scene=' + this.data._id,
			imageUrl: this.data.cover_image.tempFileURL
		}
	},
	makeCall() {
		wx.makePhoneCall({
			phoneNumber: this.data.phone,
		})
	},
	openLocation() {
		wx.openLocation({
			latitude: this.data.location.latitude,
			longitude: this.data.location.longitude,
			address: this.data.address,
			name: this.data.name,
		})
	}

})