const app = getApp()
Page({
	data: {
		StatusBar: app.globalData.StatusBar,
		CustomBar: app.globalData.CustomBar,
		Custom: app.globalData.Custom,
		list: [],
		covers: [],
		inited: false,
		swiperList: [],
	},
	onLoad: function (options) {
		app.db.collection('setting').doc('swiper').get().then(res => {
			this.setData({
				swiperList: res.data.setvalue
			})
		})
		this.getData()

	},
	onShow() {
		let that = this
		app.userInfo(function (res) {
			that.setData({
				userInfo: res
			})
		})
	},
	search(e) {
		this.setData({
			search: e.detail.value,
			list: []
		})
		this.getData()
	},
	getData() {
		if (!this.data.latitude || !this.data.longitude) {
			wx.getLocation({
				type: 'gcj02',
				success: res => {
					this.setData({
						latitude: res.latitude,
						longitude: res.longitude
					})
					this.getData()
				},
				fail: res => {
					wx.showModal({
						title: '提示',
						content: '请开启位置权限',
						showCancel: false,
						success: res => {
							wx.openSetting({
								success: res => {
									console.log(res)
									if (res.authSetting['scope.userLocation']) {
										this.getData()
									} else {
										wx.showToast({
											title: '未开启位置权限无法查询附近的门店',
											icon: 'none'
										})
									}

								},
								fail(res) {
									wx.showToast({
										title: '未开启位置权限无法查询附近的门店',
										icon: 'none'
									})
								}
							})
						}
					})
				}
			})
		} else {
			wx.showLoading({
				title: '加载中',
				mask: true
			})
			let list = this.data.list

			const _ = app.db.command
			let where = _.and([{
				status: 2,
				is_show: true,
			}])
			if (this.data.search) {
				where = where.and([
					_.or([{
						name: app.db.RegExp({
							regexp: this.data.search,
							options: 'i',
						})
					}, {
						address: app.db.RegExp({
							regexp: this.data.search,
							options: 'i',
						})
					}])
				]).and([{
					location: _.geoNear({
						geometry: app.db.Geo.Point(this.data.longitude, this.data.latitude),
					})
				}])
			} else {
				where = where.and([{
					location: _.geoNear({
						//maxDistance: 50000,
						geometry: app.db.Geo.Point(this.data.longitude, this.data.latitude),
					})
				}])
			}
			console.log(where)

			app.db.collection('shop').where(where).field({
				address: true,
				location: true,
				name: true,
				cover_image: true,
				_id: true,
			}).skip(list.length).limit(8).get().then(res => {
				wx.hideLoading()
				wx.stopPullDownRefresh()
				let covers = []

				if (list.length > 0 && res.data.length == 0) {
					return wx.showToast({
						title: '没有更多数据了',
						icon: 'none'
					})
				}

				for (let i = 0; i < res.data.length; i++) {
					res.data[i].distance = app.distance(this.data.latitude, this.data.longitude, res.data[i].location.latitude, res.data[i].location.longitude)
					list.push(res.data[i])
					covers.push(res.data[i].cover_image)
				}
				wx.cloud.getTempFileURL({
					fileList: covers,
					success: res => {
						console.log(res)
						let covers = this.data.covers
						covers = covers.concat(res.fileList)
						this.setData({
							covers,
						})
					},
					fail(res) {
						console.error(res)
					}
				})

				this.setData({
					list,
				})
			})

		}


	},

	onPullDownRefresh: function () {
		this.setData({
			list: []
		})
		this.getData()
	},
	onReachBottom: function () {
		this.getData()
	},
	doSwiper(e) {
		let item = this.data.swiperList[e.currentTarget.dataset.ind]

		if (item.bindtap == 'showToast') {
			wx.showToast({
				title: item.text,
				icon: 'none'
			})
		} else if (item.bindtap == 'toPage') {
			wx.navigateTo({
				url: item.url,
			})
		} else if (item.bindtap == 'loginToPage') {
			if (!this.data.userInfo || !this.data.userInfo._id) {
				return wx.showToast({
					title: '请先登录',
					icon: 'none',
					complete: function () {
						setTimeout(function () {
							wx.switchTab({
								url: '/pages/user/index',
							})
						}, 1500)
					}
				})
			}
			wx.navigateTo({
				url: item.url,
			})
		}
	},
	loginToPage(e) {
		if (!this.data.userInfo || !this.data.userInfo._id) {
			return wx.showToast({
				title: '请先登录',
				icon: 'none',
				complete: function () {
					setTimeout(function () {
						wx.switchTab({
							url: '/pages/user/index',
						})
					}, 1500)
				}
			})
		}
		wx.navigateTo({
			url: e.currentTarget.dataset.url,
		})
	},
	onShareAppMessage: function () {
		return {
			title: '在线预约口罩等物资',
			path: '/pages/index/index',
			imageUrl: 'https://qnimg.dqzhiyu.com/mask_share.jpg',
		}
	},
	toAppoints() {
		app.userInfo(res => {
			wx.navigateTo({
				url: '/pages/userAppoints/index',
			})
		}, res => {
			wx.showToast({
				title: '请先登录',
				icon: 'none',
				complete: res => {
					wx.switchTab({
						url: '/pages/user/index',
					})
				}
			})
		})
	}
})