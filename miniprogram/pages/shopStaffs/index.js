const app = getApp()
Page({

	data: {
		statusGroupTitle: app.globalData.statusGroupTitle,
		list: [],
		rules: {
			setting: '门店设置',
			product: '项目管理',
			appointsetting: '预约设置',
			appoints: '预约记录',
			codes: '校验规则',
		}
	},
	onLoad: function (options) {
		this.setData(options)
		this.getData()
	},
	getData() {
		wx.showLoading({
			title: '加载中',
			mask: true
		})
		app.cloud({
			name: 'shopStaffs',
			data: {
				shop_id: this.data.shop_id,
				skip:this.data.list.length
			},
			loading: true,
			def: true,
			success: res => {
				wx.stopPullDownRefresh()
				if (res.data.length == 0) {
					if (page > 1) wx.showToast({
						title: '没有更多数据了',
						icon: 'none'
					})
					return
				}
				let list = this.data.list
				list = list.concat(res.data)
				this.setData({
					list,
				})
			}
		})
	},
	onReachBottom() {
		this.getData()
	},
	onPullDownRefresh() {
		this.setData({
			list: []
		})
		this.getData()
	},
	doStaff(e) {
		let item = this.data.list[e.currentTarget.dataset.ind]
		if (item.group_id == 1) {
			return wx.showToast({
				title: '您是创始人',
				icon: 'none'
			})
		}
		let staff = app.db.collection('staff').doc(item._id).get({
			success: res => {
				res.data.nickName = item.nickName
				this.setData({
					modalName: 'staffModal',
					modalIndex: e.currentTarget.dataset.ind,
					modalData: res.data,
				})
			},
			fail: res => {
				console.error(res)
				wx.showToast({
					title: '未找到员工',
					icon: 'none'
				})
			}
		})

	},
	hideModal() {
		this.setData({
			modalName: ''
		})
	},
	onSubmit(e) {
		let data = {
			shop_id: this.data.shop_id,
			_id: this.data.modalData._id,
			op: 'set'
		}
		for (var x in this.data.rules) {
			data['rule_' + x] = e.detail.value['rule_' + x]
		}
		app.cloud({
			name: 'shopStaff',
			data,
			loading: '保存中',
			def: true,
			success: res => {
				wx.showToast({
					title: res.msg,
				})
				this.hideModal();
			}
		})
		console.log(e.detail.value)
	},
	changeGroup(e) {
		wx.showModal({
			title: '操作确认',
			content: '您确认要将其设置为' + this.data.statusGroupTitle[e.currentTarget.dataset.val].text + '吗？',
			success: res => {
				if (res.confirm) {
					let data = {
						shop_id: this.data.shop_id,
						_id: this.data.modalData._id,
						op: 'changeGroup',
						val: e.currentTarget.dataset.val
					}

					app.cloud({
						name: 'shopStaff',
						data,
						loading: '提交中',
						def: true,
						success: res => {
							wx.showToast({
								title: res.msg,
							})
							let list = this.data.list
							list[this.data.modalIndex].group_id = data.val
							this.setData({
								list
							})
							this.hideModal();
						}
					})
				}
			}
		})

	},
	onDelete(e) {
		wx.showModal({
			title: '操作确认',
			content: '您确认要删除这个员工吗？',
			success: res => {
				if (res.confirm) {
					let data = {
						shop_id: this.data.shop_id,
						_id: this.data.modalData._id,
						op: 'delete',
					}

					app.cloud({
						name: 'shopStaff',
						data,
						loading: '提交中',
						def: true,
						success: res => {
							wx.showToast({
								title: res.msg,
							})
							let list = this.data.list
							list.splice(this.data.modalIndex,1)
							this.setData({
								list
							})
							this.hideModal();
						}
					})
				}
			}
		})

	}

})