const app = getApp()
Page({
	data: {
		daily: []
	},
	onLoad: function (options) {
		this.setData(options)

		app.db.collection('product').doc(this.data._id).field({
			_id:true,
			code_id:true,
			name:true,
			price:true,
			to_home:true,
			to_shop:true,
			shop_id:true
		}).get({
			success: res => {
				if (res.data.shop_id != this.data.shop_id) {
					return this.setData({
						result: '未找到此项目'
					})
				}
				this.setData({
					product: res.data
				})
				wx.setNavigationBarTitle({
					title: res.data.name,
				})
				this.getData()
			},
			fail: res => {
				wx.hideLoading()
				this.setData({
					result: '未找到此项目'
				})
			}
		})
	},
	getData() {
		wx.showLoading({
			title: '加载中',
			mask: true
		})
		const _ = app.db.command
		app.db.collection('daily').where({
			'product_id': this.data._id,
			'date': _.gte(app.getDate(new Date())),
			'is_allow':true
		})
		.field({
			_id:true,
			date:true,
			time_begin:true,
			time_end:true,
			type:true
		})
		.skip(this.data.daily.length)
		.limit(15)
		.orderBy('date','asc')
		.get({
			success:res=>{
				wx.hideLoading()
				for(let i=0;i<res.data.length;i++){
					res.data[i].time_begin = app.getDatetime(res.data[i].time_begin);
					res.data[i].time_end = app.getDatetime(res.data[i].time_end);
				}
				let daily = this.data.daily.concat(res.data)
				this.setData({daily})
				console.log(res)
			},
			fail:res=>{
				wx.hideLoading()
				console.error(res)
			}
			
		})
	},
	onReachBottom(){
		this.getData()
	},
	showTimeline(e){
		let item = this.data.daily[e.currentTarget.dataset.ind]
		//获取timeline
		app.db.collection('timeline').where({
			daily_id:item._id
		}).field({
			_id:true,
			nums_cnt:true,
			nums_limit:true,
			nums_max:true,
			time_begin:true,
			time_end:true
		}).orderBy('time_begin','asc').get({
			success:res=>{
				console.log(res)

				this.setData({
					modalInd:e.currentTarget.dataset.ind,
					modalName:'timeline',
					timelines:res.data
				})
			},
			fail:res=>{
				console.error(res)
			}
		})
		
	},
	hideModal(){
		this.setData({
			modalName:''
		})
	}

})