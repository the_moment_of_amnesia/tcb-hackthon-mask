const app = getApp()
Page({
	data: {
		list: [],
		page: 1,
		statusShopTitle: app.globalData.statusShopTitle,
		result:'当前还没有您创建或管理门店'
	},
	toShop(e) {
		let item = this.data.list[e.currentTarget.dataset.ind]
		if (item.status != 2) {
			wx.navigateTo({
				url: '/pages/shopApply/index?_id=' + item._id,
			})
		} else {
			wx.navigateTo({
				url: '/pages/shopIndex/index?_id=' + item._id,
			})
		}
	},
	onLoad: function (options) {
		this.getData(1)
	},
	getData(page = 1) {
		let list = this.data.list
		app.cloud({
			name: 'shops',
			data: {
				skip: 20 * (page - 1),
				limit: 20
			},
			loading:'加载中',
			success:res=>{
				if(res.data.length==0){
					return wx.showToast({
					  title: '没有更多数据了',
					  icon:'none'
					})
				}
				list = list.concat(res.data)
				this.setData({
					list,
					page
				})
				wx.stopPullDownRefresh()
			}
		})
	},

	onPullDownRefresh: function () {
		this.setData({
			list: []
		})
		this.getData(1)
	},
	onReachBottom: function () {
		this.getData(this.data.page + 1)
	},
})