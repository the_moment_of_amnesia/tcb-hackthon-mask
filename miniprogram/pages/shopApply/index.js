const app = getApp()
Page({
	data: {
		subscribe: true,
		license_name: '',
		license_code: '',
		address: '',
		location: {
		},
		apply_name: '',
		apply_phone: '',
		apply_wechat: '',
		cover_image: '',
		license_image: '',
		result: '页面加载中...',
		statusShopTitle: app.globalData.statusShopTitle,
		status: 0
	},
	onLoad(options) {
		this.setData(options)
		if (options._id) {
			app.cloud({
				name: 'shopGetApply',
				data: {
					_id: options._id
				},
				success: res => {
					this.setData({
						result: ''
					})
					res.data.location = {longitude:res.data.location.coordinates[0],latitude:res.data.location.coordinates[1]}
					res.data.examine.map(function(n){
						n.time = app.getDatetime(new Date(n.time))
					})
					this.setData(res.data)
				},
				fail: res => {
					this.setData({
						result: res.code + ':' + res.msg
					})
				}
			})
		} else {
			this.setData({
				result: ''
			})
		}
	},
	onSubmit(e) {

		let data = {},
			that = this
			if(this.data._id){
				data._id = this.data._id;
			}
		let forms = {
			license_name: '营业执照名称',
			license_code: '统一社会信用代码',
			apply_name: '联系人姓名',
			apply_phone: '联系人电话',
			apply_wechat: '联系人微信'
		}
		for (let x in forms) {
			if (!this.data[x]) {
				return wx.showToast({
					title: '请填写' + forms[x],
					icon: 'none'
				})
			}
			data[x] = this.data[x]
		}

		if (!this.data.address) {
			return wx.showToast({
				title: '请选择地址',
				icon: 'none'
			})
		}
		if (!this.data.license_image) {
			return wx.showToast({
				title: '请选择营业执照',
				icon: 'none'
			})
		}
		if (!this.data.cover_image) {
			return wx.showToast({
				title: '请选择门店照片',
				icon: 'none'
			})
		}

		//处理地址
		data.address = this.data.address
		data.location = app.db.Geo.Point(this.data.location.longitude, this.data.location.latitude)
		new Promise(function (resolver, reject) {
			if (that.data.subscribe) {
				let tmpid = 'Vhv2KqnlrhpiLeEoCjsAJSht3-F6dkpYCFsAZE_OzsI'
				wx.requestSubscribeMessage({
					tmplIds: [tmpid],
					complete(res) {
						data.subscribe = true
						resolver(res[tmpid])
					}
				})
			} else {
				resolver()
			}
		}).then(res => {
			wx.showLoading({
				title: '提交中',
				mask: true
			})
			new Promise(function (resolver, reject) {
				if (that.data.license_image && that.data.license_image.substr(0, 8) != 'cloud://') {
					app.upload(that.data.license_image, 'license', res => {
						data.license_image = res.fileID
						console.log('营业执照上传成功', res)
						resolver()
					})
				} else if (that.data.license_image) {
					data.license_image = that.data.license_image
					resolver()
				} else {
					resolver()
				}
			}).then(res => {
				new Promise(function (resolver, reject) {
					if (that.data.cover_image && that.data.cover_image.substr(0,8) != 'cloud://') {
						app.upload(that.data.cover_image, 'cover', res => {
							data.cover_image = res.fileID
							console.log('门店照片上传成功', res)
							resolver()
						})
					} else if (that.data.cover_image) {
						data.cover_image = that.data.cover_image
						resolver()
					} else {
						resolver()
					}
				}).then(res => {

					return app.cloud({
						name: 'shopApply',
						data: data,
						def: true,
						success: res => {
							wx.hideLoading()
							wx.showToast({
								title: '申请已提交',
							})
							res.data.location = {longitude:res.data.location.coordinates[0],latitude:res.data.location.coordinates[1]}
							res.data.examine.map(function(n){
								n.time = app.getDatetime(new Date(n.time))
							})
							that.setData(res.data)
							
						},
						fail(res) {
							wx.hideLoading()
							wx.showToast({
								title: res.msg,
								icon: 'none'
							})
						}
					})
				})
			})
		})

	},
	chooseLocation() {
		if (this.data.status == 1 || this.data.status == 2) return
		wx.chooseLocation({
			complete: (res) => {
				this.setData({
					location: {
						longitude: res.longitude,
						latitude: res.latitude,
					},
					address: res.address
				})
			},
		})
	},
	onCancel() {
		wx.showModal({
			title: '操作确认',
			content: '您确认要取消申请吗',
			success: res => {
				if (res.confirm) {
					app.cloud({
						name: 'shopCancelApply',
						data: {
							_id: this.data._id
						},
						def: true,
						loading: '取消中',
						success: res => {
							wx.showToast({
								title: res.msg,
							})
							res.data.location = {longitude:res.data.location.coordinates[0],latitude:res.data.location.coordinates[1]}
							res.data.examine.map(function(n){
								n.time = app.getDatetime(new Date(n.time))
							})
							this.setData(res.data)
							
						}
					})
				}
			}
		})

	},
	upload(e) {
		console.log(e)
		wx.chooseImage({
			count: 1,
			sizeType: ['compressed'],
			success: chooseResult => {
				this.setData({
					[e.currentTarget.dataset.key]: chooseResult.tempFilePaths[0],

				})
			},
		})
	},
	showImg(e) {
		if (this.data[e.currentTarget.dataset.key].substr(0, 8) == 'cloud://') {
			wx.cloud.getTempFileURL({
				fileList: [{
					fileID: this.data[e.currentTarget.dataset.key],
					maxAge: 60 * 60, // one hour
				}]
			}).then(res => {
				console.log(res)
				wx.previewImage({
					urls: res.fileList.map(function (n) {
						return n.tempFileURL
					}),
				})
			})
		} else {
			wx.previewImage({
				urls: [this.data[e.currentTarget.dataset.key]],
			})
		}
	},
	subscribe(e) {
		this.setData({
			subscribe: e.detail.value.length > 0
		})
	},
	onInput(e) {
		this.setData({
			[e.currentTarget.id]: e.detail.value
		})
	}
})